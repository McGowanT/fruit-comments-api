package com.fruitcomments.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;

import com.fruitcomments.api.entities.Fruit;
import com.fruitcomments.api.service.FruitService.FruitService;

@RestController
@RequestMapping("api/fruits")
public class FruitController 
{
	
	@Autowired
	
	protected FruitService fruitService;
	
	@GetMapping("")
	public List<Fruit> getAll()
		{
			return fruitService.getAll();
		
		}
}
