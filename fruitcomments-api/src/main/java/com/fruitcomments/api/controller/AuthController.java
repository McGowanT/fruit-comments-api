package com.fruitcomments.api.controller;

import org.springframework.web.bind.annotation.*;

import com.fruitcomments.api.viewmodels.LoginViewModel;

@RestController
@RequestMapping("api/auth")
public class AuthController 
{
	
	@GetMapping("/hello")
	public String hello()
	{
		return "Hello from the OTHER SIDE.";
	}
	
	@GetMapping("/sayHello")
	public String sayHello(@RequestParam("firstName")String firstName, @RequestParam("lastName")java.util.Optional<String> lastName)
	{
		return String.format("Hello %s %s", firstName, lastName);
	}
	
	@GetMapping("user/{username}")
	public LoginViewModel getUser(@PathVariable("username") String username)
	{
		LoginViewModel data = new LoginViewModel (username, "Never send password back to user");
			return data;
	}
	
	@PostMapping("/login")
	public String login(@RequestBody LoginViewModel credentials)
	{
		return String.format("Fake-Token-%s-%s", credentials.getUsername(), credentials.getPassword());
	}
}
